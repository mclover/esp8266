esp8266
# goal:
- rfid reader on esp8266
- after esp8266 reads tags, data is send to a mqtt broker
- mqtt broker gets tag, does something special with this tag
- wol

# rfid reader on esp8266
see -> https://github.com/Jorgen-VikingGod/ESP8266-MFRC522

# mqtt broker
for esp8266 installing PubSubClient.h
for the server: probably docker mqtt image -> https://hub.docker.com/_/eclipse-mosquitto/

# mqtt does special stuff
probably something with node-red which subscribes the mqtt topic, then 
more cool stuff if influxdb, grafana, etc.

# wol
per rfid tag i create a list of mac addreses. when the rfid tag is scanned and
send to the mqtt broker, there will be a wol package that gets transmitted
to every mac address for that rfid tag. prob. a little db (redis?)

# plus points
django web frontend for rfid tag registration, editing mac addresses and status
of devices!